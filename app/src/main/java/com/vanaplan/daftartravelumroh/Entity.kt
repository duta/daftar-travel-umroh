package com.vanaplan.daftartravelumroh

data class User(val name: String, val address: String)

data class Kota(val nama_kota: String)

data class Travel(val penyelenggara:String, val alamat:String)

data class Product(val title:String, val price:Number)